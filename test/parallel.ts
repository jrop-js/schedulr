import test = require('tape')
import parallel from '../src/parallel'
import {Fn} from '../src'

let curr = 0
const myFunc = parallel(3, async (checker: Fn<any>) => {
	++curr
	await new Promise(y => setTimeout(y, 150))
	checker()
	--curr
})

test('parallel', async t => {
	t.plan(11)
	await Promise.all(
		new Array(10)
			.fill(0)
			.map(async () => await myFunc(() => t.assert(curr <= 3)))
	)
	t.equal(curr, 0)
})
