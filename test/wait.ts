import test = require('tape')
import wait from '../src/wait'
import {Fn} from '../src'

const myFunc = wait(50, () => null)

test('wait', async t => {
	t.plan(1)
	const start = new Date().getTime()
	await Promise.all(new Array(10).fill(0).map(() => myFunc()))
	const end = new Date().getTime()
	t.assert(end - start > 50 * 10)
})
