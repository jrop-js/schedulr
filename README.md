schedulr
========

> Schedules invocation of a function based on constraints

[![build status](https://gitlab.com/jrop-js/schedulr/badges/master/build.svg)](https://gitlab.com/jrop-js/schedulr/commits/master)

## Installation

```sh
npm install schedulr
```

## Usage

Import:

```js
import {parallel, wait} from 'schedulr'
```

Limit a function to being running at most 3 times concurrently:

```js
const myFunc = parallel(3, async () => {
	// ...
})

await myFunc()
await myFunc()
await myFunc()
await myFunc() // only 3 will run at once, but gets invoked 4 times

const myFunc2 = wait(250, async () => {
	// ...
})

await myFunc2()
await myFunc2() // will get invoked 250ms after first invocation
```

## License

ISC License (ISC)
Copyright 2017 Jonathan Apodaca <jrapodaca@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
