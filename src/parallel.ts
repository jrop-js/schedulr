import {Fn} from './index'

enum Status {
	Queued,
	Started,
	Done,
}
type Deferred = {
	promise: Promise<any>
	resolve: Fn<any>
	reject: Fn<any>
}
type Task = {
	fn: Fn<any>
	status: Status
	deferred?: Deferred
}

function defer(): Deferred {
	const deferred: any = {}
	deferred.promise = new Promise((y, n) => {
		deferred.resolve = y
		deferred.reject = n
	})
	return deferred
}

class Pool {
	max: number
	queue: Task[]

	constructor(max: number) {
		this.max = max
		this.queue = []
	}

	collect(t: Task) {
		this.queue = this.queue.filter(task => task != t)
		this.tick()
	}

	tick() {
		const nRunning = this.queue.filter(t => t.status == Status.Started).length
		const pending = this.queue.filter(t => t.status == Status.Queued)
		if (nRunning < this.max && pending.length > 0) {
			const [next] = pending
			next.deferred.promise
				.then(() => this.collect(next))
				.catch(() => this.collect(next))
			next.status = Status.Started
			Promise.resolve(next.fn()).then(
				next.deferred.resolve,
				next.deferred.reject
			)
		}
	}

	run<T>(fn: Fn<T>): Promise<T> {
		const task = {
			fn,
			status: Status.Queued,
			deferred: defer(),
		}
		this.queue.push(task)
		this.tick()
		return task.deferred.promise
	}
}

export default function parallel<T>(max: number, fn: Fn<T>): Fn<Promise<T>> {
	const pool = new Pool(max)
	return (...args) => pool.run(() => fn(...args))
}
