import parallel from './parallel'
import wait from './wait'

export type Fn<T> = (...args) => T
export {parallel, wait}
