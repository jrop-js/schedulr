import {Fn} from './index'

class TimePool {
	private _next: number
	private _wait: number
	constructor(wait: number) {
		this._next = 0
		this._wait = wait
	}

	async wait() {
		const now = new Date().getTime()
		this._next = Math.max(this._next, now) + this._wait
		await new Promise(y => setTimeout(y, this._next - now))
	}
}

export default function wait<T>(ms: number, fn: Fn<T>): Fn<Promise<T>> {
	const waiter = new TimePool(ms)
	return async (...args) => {
		await waiter.wait()
		return await Promise.resolve(fn(...args))
	}
}
